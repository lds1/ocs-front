import {render, screen} from '@testing-library/react'
import Bouton from '@/components/Button'

describe('Button', () => {
    it('renders a button with message test', () => {
        render(<Bouton message="test"/>)

        screen.getByText('test')
    })
})