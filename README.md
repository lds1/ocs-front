# OCS Front

## Getting started

Pour installer
```
npm install -g pnpm
pnpm install
```

Pour lancer en dev
```
pnpm dev
```

## Test

Pour lancer les test pour le dev avec un watcher
```
pnpm test
```

Pour lancer les test avec coverage
```
pnpm test:ci
```