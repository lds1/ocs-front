import styles from '@/styles/Bouton.module.scss'

type Props = {
    message: string;
};

export default function Bouton({message}: Props) {
    return <div className={styles.bouton}>{message}</div>
}